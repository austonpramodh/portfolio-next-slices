# Prismic NextJS Slice Machine Webpage

<!--
initial config
starting dev server
adding new slices
 -->
## Starting the development server

Command - `npm run dev`

## Adding a new Slice

Command - `npm run creat:slice`


## Slice Machine Storybook Screenshot issue
Replace the function `createScreenshotUrl` in file `node_modules\slice-machine-ui\lib\utils.js` with the following

```js
export const createScreenshotUrl = ({ storybook, sliceName, variation }) => {
  return `${storybook}/iframe.html?id=${sliceName.toLowerCase()}&viewMode=story`
}
```