module.exports = {
    purge: ["./src/**/*.{js,ts,jsx,tsx}"],
    darkMode: false, // or 'media' or 'class'
    theme: {
        container: {
            center: true,
        },
        fontFamily: {
            lato: ["Lato", "sans-serif"],
        },
        extend: {
            width: {
                "12/9": "130%",
            },
            margin: {
                "-1/2-screen": "-50vw",
                "-2/12": "-15%",
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
    important: true,
};
