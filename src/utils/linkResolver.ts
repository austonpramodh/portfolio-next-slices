interface Document {
    type: string;
    uid: string;
}

export const linkResolver = (doc: Document) => {
    if (doc.type === "blog_post") {
        return `/blogs/${doc.uid}`;
    }
    return "/";
};

// Additional helper function for Next/Link components
export const hrefResolver = (doc: Document) => {
    if (doc.type === "blog_post") {
        return "/blogs/[uid]";
    }
    return "/";
};
