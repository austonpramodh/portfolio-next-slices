import { ImageLoader } from "next/image";

export const imageLoader: ImageLoader = ({ src, width, quality }) => {
    const url = new URL(src);
    const urlSearchParams = (url.search ? `${url.search}&` : "?") + `w=${width}&q=${quality || 75}`;
    const finalUrl = `${url.origin}${url.pathname}${urlSearchParams}`;
    return finalUrl;
};
