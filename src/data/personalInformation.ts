import Prismic from "@prismicio/client";

import { Client } from "../lib/prismic-client";

export interface PersonalInformation {
    id: string;
    uid: string;
    type: string;
    first_publication_date: string;
    last_publication_date: string;
    data: {
        name: string;
        profile_picture: {
            dimensions: {
                width: number;
                height: number;
            };
            alt: string | null;
            copyright: string | null;
            url: string;
        };
        self_introduction: string;
    };
}

export const getPersonalInfromation = async (): Promise<PersonalInformation | null> => {
    const client = Client();

    const docs = await client.query([Prismic.Predicates.at("document.type", "personal_infora")], {
        fetch: ["name", "profile_picture", "self_introduction"].map((d) => `personal_infora.${d}`),
    });

    return (docs.results[0] as PersonalInformation) || null;
};
