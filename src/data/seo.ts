import Prismic from "@prismicio/client";

import { Client } from "../lib/prismic-client";

export interface SEOInformation {
    id: string;
    uid: string;
    type: string;
    first_publication_date: string;
    last_publication_date: string;
    data: {
        meta_description: string;
        meta_image: {
            dimensions: {
                width: number;
                height: number;
            };
            alt: string | null;
            copyright: string | null;
            url: string;
        };
        site_title: string;
        meta_keywords: { keyword: string }[];
    };
}

export const getSEOInformation = async (): Promise<SEOInformation | null> => {
    const client = Client();

    const docs = await client.query([Prismic.Predicates.at("document.type", "seo")]);

    return (docs.results[0] as SEOInformation) || null;
};
