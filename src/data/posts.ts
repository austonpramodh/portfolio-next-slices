import Prismic from "@prismicio/client";

import { Client } from "../lib/prismic-client";
import { SliceType } from "../slices/getSliceComponent";

export interface PostWithoutBody {
    id: string;
    uid: string;
    type: string;
    first_publication_date: string;
    last_publication_date: string;
    data: {
        title: string;
        short_title: string;
        introduction: string;
    };
}

export const getAllPostsWithoutBody = async (): Promise<PostWithoutBody[]> => {
    const client = Client();

    const docs = await client.query([Prismic.Predicates.at("document.type", "blog_post")], {
        orderings: "[blog_post.first_publication_date desc]",
        fetch: ["short_title", "title", "introduction"].map((d) => `blog_post.${d}`),
    });

    return docs.results as PostWithoutBody[];
};

export interface Post extends PostWithoutBody {
    data: {
        body: SliceType[];
    } & PostWithoutBody["data"];
}

export const getPostByUid = async (uid: string): Promise<Post | undefined> => {
    const client = Client();

    const doc = await client.getByUID("blog_post", uid, {});

    return (doc as Post) || null;
};
