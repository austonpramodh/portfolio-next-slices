import Prismic from "@prismicio/client";

export const apiEndpoint = "https://auston-test.cdn.prismic.io/api/v2";
export const accessToken = "";

// Client method to query documents from the Prismic repo
export const Client = (req?: any) => Prismic.client(apiEndpoint, createClientOptions(req, accessToken));

const createClientOptions = (req: null | any, prismicAccessToken?: string) => {
    return {
        req: req || undefined,
        accessToken: prismicAccessToken,
    };
};
