import React from "react";

import getSliceComponent, { SliceType } from "./getSliceComponent";

interface SliceZoneProps {
    slices: SliceType[];
}

const SliceZone: React.FunctionComponent<SliceZoneProps> = ({ slices }) => (
    <>{slices.map((slice, index) => getSliceComponent(slice, index))}</>
);

export default SliceZone;
