import React from "react";

import ImageSlice, { ImageSliceType, ImageSliceProps } from "./components/ImageSlice";
import TextSlice, { TextSliceProps, TextSliceType } from "./components/TextSlice";
import EmbedLinkSlice, { EmbedLinkSliceProps, EmbedLinkSliceType } from "./components/EmbedLink";
import CodeSnippetSlice, { CodeSnippetSliceProps, CodeSnippetSliceType } from "./components/CodeSnippet";
import QuoteSlice, { QuoteSliceProps, QuoteSliceType } from "./components/QuoteSlice";
import RichTextSlice, { RichTextSliceProps, RichTextSliceType } from "./components/RichText";

export type SliceType = { slice_type: string } & (
    | TextSliceProps
    | ImageSliceProps
    | EmbedLinkSliceProps
    | CodeSnippetSliceProps
    | QuoteSliceProps
    | RichTextSliceProps
);

const getSliceComponent = (slice: SliceType, index: number) => {
    switch (slice.slice_type) {
        case TextSliceType:
            return <TextSlice key={slice.slice_type + index} {...slice} />;
        case ImageSliceType:
            return <ImageSlice key={slice.slice_type + index} {...slice} />;
        case EmbedLinkSliceType:
            return <EmbedLinkSlice key={slice.slice_type + index} {...slice} />;
        case CodeSnippetSliceType:
            return <CodeSnippetSlice key={slice.slice_type + index} {...slice} />;
        case QuoteSliceType:
            return <QuoteSlice key={slice.slice_type + index} {...slice} />;
        case RichTextSliceType:
            return <RichTextSlice key={slice.slice_type + index} {...slice} />;
        default:
            // eslint-disable-next-line no-console
            console.log(`Slice not found for SliceType: ${slice["slice_type"]}`, slice);
            return (
                <div key={"unknownSlice" + index}>
                    Slice not found for SliceType : {JSON.stringify(slice["slice_type"])}
                </div>
            );
    }
};

export default getSliceComponent;
