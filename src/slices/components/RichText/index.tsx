import clsx from "clsx";
import React from "react";
import { RichText, RichTextBlock } from "prismic-reactjs";
export const RichTextSliceType = "rich_text" as const;

export interface RichTextSliceProps {
    slice_type: typeof RichTextSliceType;
    primary: {
        richText: RichTextBlock[];
        fullWidth: boolean;
    };
}

const RichTextSlice: React.FunctionComponent<RichTextSliceProps> = (props) => {
    const { primary } = props;

    return (
        <section className={clsx(!primary.fullWidth && "blog-container", "mb-4")}>
            <RichText render={primary.richText} />
        </section>
    );
};

export default RichTextSlice;
