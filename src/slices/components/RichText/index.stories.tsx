import React from "react";
import { Meta, Story } from "@storybook/react";

import model from "./model.json";
import mocks from "./mocks.json";

import Component, { RichTextSliceProps, RichTextSliceType } from ".";

const DefaultStory = {
    title: `${model.name}`,
} as Meta;

export default DefaultStory;

interface ComponentArgs {
    fullWidth: boolean;
}
const firstMock = mocks[0];

const Template: Story<ComponentArgs> = ({ fullWidth }) => {
    const props: RichTextSliceProps = {
        ...firstMock,
        primary: {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            //@ts-expect-error
            richText: firstMock.primary.richText,
            fullWidth: fullWidth,
        },
        slice_type: RichTextSliceType,
    };

    return <Component {...props} />;
};

export const Primary = Template.bind({});

Primary.storyName = `Default Slice`;

Primary.args = {
    fullWidth: firstMock.primary.fullWidth,
};

Primary.argTypes = {
    fullWidth: {
        name: "Full Width",
        description: "Take full width for Rich Text Block",
        defaultValue: false,
        control: "boolean",
    },
};
