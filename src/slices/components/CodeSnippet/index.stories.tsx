import React from "react";
import { Meta, Story } from "@storybook/react";

import model from "./model.json";
import mocks from "./mocks.json";

import Component, { CodeSnippetSliceType, CodeSnippetSliceProps } from "./";

const DefaultStory = {
    title: `${model.name}`,
} as Meta;

export default DefaultStory;

interface ComponentArgs {
    codeSnippet: string;
    showLineNumber: boolean;
    language: string;
    fullWidth: boolean;
}

const Template: Story<ComponentArgs> = (args: ComponentArgs) => {
    const props: CodeSnippetSliceProps = {
        slice_type: CodeSnippetSliceType,
        primary: {
            codeSnippet: args.codeSnippet,
            language: args.language,
            showLineNumber: args.showLineNumber,
            fullWidth: args.fullWidth,
        },
    };

    return <Component {...props} />;
};

export const Primary = Template.bind({});

Primary.storyName = `Default Slice`;

const firstMock = mocks[0];

Primary.args = {
    codeSnippet: firstMock.primary.codeSnippet,
    showLineNumber: firstMock.primary.showLineNumber,
    language: firstMock.primary.language,
    fullWidth: firstMock.primary.fullWidth,
};

Primary.argTypes = {
    codeSnippet: {
        name: "Code Snippet",
        description: "Code Snippet to be shown",
        defaultValue: 'Console.log("hello");',
        control: "text",
    },
    showLineNumber: {
        name: "Show Line Numbers",
        description: "Show Line Numbers on Code Snippet",
        defaultValue: true,
        control: "boolean",
    },
    language: {
        name: "Language",
        description: "Language of the Code Snippet",
        defaultValue: null,
        control: {
            type: "inline-radio",
            options: [
                model.variations[0].primary.language.config.placeholder,
                ...model.variations[0].primary.language.config.options,
            ],
        },
    },
    fullWidth: {
        name: "Full Width",
        description: "Take full width for Code Snippet",
        defaultValue: false,
        control: "boolean",
    },
};
