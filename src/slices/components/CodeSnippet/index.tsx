import clsx from "clsx";
import React from "react";
import SyntaxHighlighter from "react-syntax-highlighter";
import githubGist from "react-syntax-highlighter/dist/cjs/styles/hljs/github-gist";

export const CodeSnippetSliceType = "code_snippet" as const;

export interface CodeSnippetSliceProps {
    slice_type: typeof CodeSnippetSliceType;
    primary: { codeSnippet: string; showLineNumber: boolean; language: string; fullWidth: boolean };
}

const CodeSnippetSlice: React.FunctionComponent<CodeSnippetSliceProps> = (props) => {
    const { primary } = props;
    const { codeSnippet, language, showLineNumber, fullWidth } = primary;
    return (
        <section className={clsx(!fullWidth && "blog-container", "mb-4")}>
            <SyntaxHighlighter
                language={language ? language.toLowerCase() : undefined}
                style={githubGist}
                showLineNumbers={showLineNumber}
            >
                {codeSnippet}
            </SyntaxHighlighter>
        </section>
    );
};
export default CodeSnippetSlice;
