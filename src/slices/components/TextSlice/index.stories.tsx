import React from "react";
import { Meta, Story } from "@storybook/react";

import model from "./model.json";
import mocks from "./mocks.json";

import Component, { TextSliceType, TextSliceProps } from ".";

const DefaultStory = {
    title: `${model.name}`,
} as Meta;

export default DefaultStory;

interface ComponentArgs {
    text: string;
    fullWidth: boolean;
}

const Template: Story<ComponentArgs> = (args: ComponentArgs) => {
    const props: TextSliceProps = {
        slice_type: TextSliceType,
        primary: {
            fullWidth: args.fullWidth,
            text: args.text,
        },
    };

    return <Component {...props} />;
};

export const Primary = Template.bind({});

Primary.storyName = `Default Slice`;

const firstMock = mocks[0];

Primary.args = {
    text: firstMock.primary.text,
    fullWidth: firstMock.primary.fullWidth,
};

Primary.argTypes = {
    text: {
        name: "Text",
        control: "text",
        defaultValue: null,
        description: "Text Input for the Slice",
    },
    fullWidth: {
        name: "Full Width",
        description: "Take full width for Text Block",
        defaultValue: false,
        control: "boolean",
    },
};
