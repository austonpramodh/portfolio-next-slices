import clsx from "clsx";
import React from "react";

export const TextSliceType = "text_slice" as const;

export interface TextSliceProps {
    slice_type: typeof TextSliceType;
    primary: {
        text: string;
        fullWidth: boolean;
    };
}

const TextSlice: React.FunctionComponent<TextSliceProps> = (slice) => (
    <section className={clsx(!slice.primary.fullWidth && "blog-container", "mb-4")}>
        <p className={clsx("text-base")}>{slice.primary.text}</p>
    </section>
);

export default TextSlice;
