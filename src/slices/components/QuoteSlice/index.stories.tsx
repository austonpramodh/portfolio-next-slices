import React from "react";
import { Meta, Story } from "@storybook/react";

import model from "./model.json";
import mocks from "./mocks.json";

import Component, { QuoteSliceType, QuoteSliceProps } from ".";

const DefaultStory = {
    title: `${model.name}`,
} as Meta;

export default DefaultStory;

interface ComponentArgs {
    quote: string;
}

const Template: Story<ComponentArgs> = (args: ComponentArgs) => {
    const props: QuoteSliceProps = {
        slice_type: QuoteSliceType,
        primary: {
            quote: args.quote,
        },
    };

    return <Component {...props} />;
};

export const Primary = Template.bind({});

Primary.storyName = `Default Slice`;

const firstMock = mocks[0];

Primary.args = {
    quote: firstMock.primary.quote,
};

Primary.argTypes = {
    quote: {
        name: "Quote",
        control: "text",
        defaultValue: null,
        description: "Text Input for the Slice",
    },
};
