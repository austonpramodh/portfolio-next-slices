import clsx from "clsx";
import React from "react";

export const QuoteSliceType = "quote_slice" as const;

export interface QuoteSliceProps {
    slice_type: typeof QuoteSliceType;
    primary: {
        quote: string;
    };
}

const QuoteSlice: React.FunctionComponent<QuoteSliceProps> = (slice) => {
    return (
        <section className={clsx("blog-container", "quote-section", "mb-4")}>
            <blockquote
                className={clsx(
                    "quote-text",
                    "lg:w-12/9",
                    "lg:-ml-2/12",
                    "text-lg",
                    "italic",
                    "font-semibold",
                    "subpixel-antialiased",
                    "text-gray-500",
                    "border-0",
                    "bg-white",
                )}
            >
                {slice.primary.quote}
            </blockquote>
        </section>
    );
};

export default QuoteSlice;
