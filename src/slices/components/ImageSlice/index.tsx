import React from "react";
import clsx from "clsx";
import Image from "next/image";

import { imageLoader } from "../../../utils/imageLoader";

export const ImageSliceType = "image_slice" as const;

export interface ImageSliceProps {
    slice_type: typeof ImageSliceType;
    primary: {
        image: {
            dimensions: {
                width: number;
                height: number;
            };
            alt: string | null;
            copyright: string | null;
            url: string;
        };
        quote: string | null;
        showQuote: boolean;
        width: string;
    };
}

const ImageSlice: React.FunctionComponent<ImageSliceProps> = (props) => {
    const [windowWidth, setWindowWidth] = React.useState(500);

    const { primary } = props;

    const widthType = primary.width || "Normal Width";

    const isNormalWidth = widthType === "Normal Width";
    const isMediumWidth = widthType === "Medium Width";
    const isFullWidth = widthType === "Full Width";

    React.useEffect(() => {
        if (!isFullWidth) return;

        const listener = () => {
            const newWidth = window.innerWidth;
            setWindowWidth(newWidth);
        };

        if (window) {
            listener();
            window.addEventListener("resize", listener);
        }
        return () => {
            if (window) window.removeEventListener("resize", listener);
        };
    }, [isFullWidth, setWindowWidth]);

    //TODO: get maxWidth from tailwind
    const maxWidth = 680;
    //Medium width images get 30% extra width than normal so - 1.3 factor
    const getImageWidth = () => (isMediumWidth ? Math.floor(maxWidth * 1.3) : maxWidth);

    const getImageHeight = () =>
        //If its fullscreen image, calculate based on the window width,
        //else calculate based on calculated width
        (isFullWidth ? windowWidth : getImageWidth()) *
        (primary.image.dimensions.height / primary.image.dimensions.width);

    return (
        <section
            className={clsx({
                "mb-4": true,
                "blog-container": isNormalWidth || isMediumWidth,
                "image-slice-medium-width": isMediumWidth,
                relative: true,
            })}
        >
            <div
                className={clsx({
                    "w-screen": isFullWidth,
                    "max-w-screen": isFullWidth,
                    relative: isFullWidth,
                    "inset-x-2/4": isFullWidth,
                    "-mx-1/2-screen": isFullWidth,
                })}
                style={{
                    height: isFullWidth ? getImageHeight() : "",
                }}
            >
                {isFullWidth && <Image loader={imageLoader} src={primary.image.url} layout="fill" />}
                {!isFullWidth && (
                    <Image
                        loader={imageLoader}
                        src={primary.image.url}
                        layout="responsive"
                        width={getImageWidth()}
                        height={getImageHeight()}
                        className={clsx({
                            "w-full": isNormalWidth || isFullWidth,
                            "lg:max-w-none": isMediumWidth,
                            "lg:w-12/9": isMediumWidth,
                            "lg:-ml-2/12": isMediumWidth,
                        })}
                    />
                )}
            </div>

            {primary.showQuote && (
                <p className={clsx("text-center", "text-sm", "italic", "text-gray-700")}>{primary.quote}</p>
            )}
        </section>
    );
};
export default ImageSlice;
