import React from "react";
import { Meta, Story } from "@storybook/react";

import model from "./model.json";
import mocks from "./mocks.json";

import Component, { ImageSliceType, ImageSliceProps } from "./";

const DefaultStory = {
    title: `${model.name}`,
} as Meta;

export default DefaultStory;

interface ComponentArgs {
    imageUrl: string;
    imageWidth: number;
    imageheight: number;
    alt: string | null;
    copyright: string | null;
    quote: string | null;
    showQuote: boolean;
    width: string;
}

const Template: Story<ComponentArgs> = (args: ComponentArgs) => {
    const props: ImageSliceProps = {
        slice_type: ImageSliceType,
        primary: {
            image: {
                url: args.imageUrl,
                alt: args.alt,
                copyright: args.copyright,
                dimensions: {
                    height: args.imageheight,
                    width: args.imageWidth,
                },
            },
            quote: args.quote,
            showQuote: args.showQuote,
            width: args.width,
        },
    };

    return <Component {...props} />;
};

export const Primary = Template.bind({});

Primary.storyName = `Default Slice`;

const firstMock = mocks[0];

Primary.args = {
    alt: firstMock.primary.image.alt,
    copyright: firstMock.primary.image.copyright,
    imageUrl: firstMock.primary.image.url,
    imageWidth: firstMock.primary.image.dimensions.width,
    imageheight: firstMock.primary.image.dimensions.height,
    quote: firstMock.primary.quote,
    showQuote: firstMock.primary.showQuote,
    width: firstMock.primary.width,
};

Primary.argTypes = {
    alt: {
        name: "Alternative Text",
        description: "Alternative Text is a word or phrase that can describe what an image shows.",
        defaultValue: null,
        control: "text",
    },
    copyright: {
        name: "Copyright Text",
        description:
            "A copyright notice is a short line of text that lets the public know that your work is protected by copyright law and is not to be copied.",
        defaultValue: null,
        control: "text",
    },
    imageUrl: {
        name: "Copyright Text",
        description: "URL link for the Image to be rendered",
        defaultValue: null,
        control: "text",
    },
    imageWidth: {
        name: "Image Width",
        description: "Width of the Image to be rendered",
        defaultValue: null,
        control: "number",
    },
    imageheight: {
        name: "Image Height",
        description: "Height of the Image to be rendered",
        defaultValue: null,
        control: "number",
    },
    quote: {
        name: "Quote",
        description: "Quote to be rendered below Image",
        defaultValue: null,
        control: "text",
    },
    showQuote: {
        name: "Show Quote",
        description: "Show Quote below the image",
        defaultValue: null,
        control: "boolean",
    },
    width: {
        name: "Width",
        description: "Width Code Snippet to be shown",
        defaultValue: null,
        control: {
            type: "inline-radio",
            options: [
                model.variations[0].primary.width.config.placeholder,
                ...model.variations[0].primary.width.config.options,
            ],
        },
    },
};
