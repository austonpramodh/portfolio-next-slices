import React from "react";
import { Meta, Story } from "@storybook/react";

import model from "./model.json";
import mocks from "./mocks.json";

import Component, { EmbedLinkSliceProps, EmbedLinkSliceType } from "./";

const DefaultStory = {
    title: `${model.name}`,
} as Meta;

export default DefaultStory;

interface ComponentArgs {
    embedLink: string;
    fullWidth: boolean;
}

const Template: Story<ComponentArgs> = (args: ComponentArgs) => {
    const props: EmbedLinkSliceProps = {
        slice_type: EmbedLinkSliceType,
        primary: {
            embedLink: { link_type: "web", url: args.embedLink },
            fullWidth: args.fullWidth,
        },
    };

    return <Component {...props} />;
};

export const Primary = Template.bind({});

Primary.storyName = `Default Slice`;

const firstMock = mocks[0];

Primary.args = {
    embedLink: firstMock.primary.embedLink.url,
    fullWidth: firstMock.primary.fullWidth,
};

Primary.argTypes = {
    embedLink: {
        name: "Embed Link",
        description: "Link of the Item to be embedded",
        defaultValue: "https://gist.github.com/akhilanandbv003/fd4f3898bb7b9c36f7b9a8c198e01548",
        control: "text",
    },
    fullWidth: {
        name: "Full Width",
        description: "Take full width for Code Snippet",
        defaultValue: false,
        control: "boolean",
    },
};
