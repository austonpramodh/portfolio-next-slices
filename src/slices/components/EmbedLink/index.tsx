import clsx from "clsx";
import React from "react";
import Embed from "react-embed";
export const EmbedLinkSliceType = "embed_link" as const;

export interface EmbedLinkSliceProps {
    slice_type: typeof EmbedLinkSliceType;
    primary: {
        embedLink: {
            link_type: string;
            url: string;
        };
        fullWidth: boolean;
    };
}

const EmbedLinkSlice: React.FunctionComponent<EmbedLinkSliceProps> = (props) => {
    const { primary } = props;
    const { fullWidth, embedLink } = primary;
    const [isMounted, setMounted] = React.useState(false);

    React.useEffect(() => {
        setMounted(true);
        return () => setMounted(false);
    }, []);

    return (
        <section className={clsx(!fullWidth && "blog-container", "mb-4")}>
            {isMounted && <Embed url={embedLink.url} />}
        </section>
    );
};
export default EmbedLinkSlice;
