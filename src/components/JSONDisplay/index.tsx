import { Button } from "@geist-ui/react";
import clsx from "clsx";
import React from "react";

import { jsonSyntaxHighlight } from "../../utils/syntaxHighlighter";

interface JSONDisplayProps {
    data: unknown;
}

const JSONDisplay: React.FunctionComponent<JSONDisplayProps> = ({ data }) => {
    const [show, setShow] = React.useState(false);
    return (
        <section className={clsx("border-t-2", "mt-2", "blog-container", "text-center")}>
            <Button className={"my-4"} onClick={() => setShow(!show)}>
                {show ? "Hide" : "Show"} Data JSON
            </Button>
            <pre
                className={clsx(!show && "hidden")}
                dangerouslySetInnerHTML={{ __html: jsonSyntaxHighlight(JSON.stringify(data, undefined, 4)) }}
            />
        </section>
    );
};

export default JSONDisplay;
