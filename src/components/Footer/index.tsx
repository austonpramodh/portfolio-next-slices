import clsx from "clsx";
import Link from "next/link";
import React from "react";

interface FooterProps {
    fullName: string;
}

const Footer: React.FunctionComponent<FooterProps> = ({ fullName }) => {
    return (
        <div className={clsx("blog-container")}>
            <p className={clsx("text-center", "border-t-2")}>
                Proudly published by <br className={"md:hidden"} />
                <Link href="/">
                    <a href="/" target="_blank" rel="noopener noreferrer" className="text-black">
                        <strong>
                            <em>{fullName}</em>
                        </strong>
                    </a>
                </Link>
                <br />
            </p>
        </div>
    );
};

export default Footer;
