import Head from "next/head";
import React from "react";

import { siteTitleConstant } from "../../constants/siteMeta";

interface SEOProps {
    siteTitle?: string;
    pageTitle?: string;
    keywords?: string[];
}

const SEO: React.FunctionComponent<SEOProps> = ({ siteTitle = siteTitleConstant, pageTitle, keywords = [] }) => {
    if (keywords.length === 0) {
        // eslint-disable-next-line no-console
        console.warn("Add Keywords to the site Head");
    }

    return (
        <Head>
            <title>{`${pageTitle ? `${pageTitle} |` : ""} ${siteTitle}`}</title>
            <meta charSet="utf-8" />
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <meta name="keywords" content={keywords.join(",")}></meta>
        </Head>
    );
};

export default SEO;
