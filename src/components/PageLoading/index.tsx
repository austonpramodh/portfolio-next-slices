import { Loading } from "@geist-ui/react";
import clsx from "clsx";
import Head from "next/head";
import React from "react";

interface PageLoadingProps {
    siteTitle?: string;
}

const PageLoading: React.FunctionComponent<PageLoadingProps> = ({ siteTitle }) => {
    const finalSiteTitle = siteTitle || "Site Title";
    return (
        <div className={clsx("min-w-screen", "min-h-screen", "flex", "items-center")}>
            <Head>
                <title>Loading... | {finalSiteTitle}</title>
            </Head>
            <Loading>Loading</Loading>
        </div>
    );
};

export default PageLoading;
