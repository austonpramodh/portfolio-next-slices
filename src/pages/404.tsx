import { Link } from "@geist-ui/react";
import clsx from "clsx";
import { GetStaticProps } from "next";
import NextLink from "next/link";
import React from "react";

import SEO from "../components/SEO";
import { siteTitleConstant } from "../constants/siteMeta";
import { getSEOInformation, SEOInformation } from "../data/seo";

interface NotFoundPageProps {
    seoInformation: SEOInformation | null;
}

const NotFoundPage: React.FunctionComponent<NotFoundPageProps> = ({ seoInformation }) => {
    const siteTitle = seoInformation?.data.site_title || siteTitleConstant;

    return (
        <>
            <SEO
                siteTitle={siteTitle}
                keywords={seoInformation?.data.meta_keywords.map((k) => k.keyword)}
                pageTitle={"404 Not Found"}
            />
            <div
                className={clsx(
                    "min-h-screen",
                    "min-w-screen",
                    "bg-gray",
                    "flex",
                    "justify-center",
                    "align-center",
                    "flex-col",
                    "items-center",
                )}
            >
                <div className={clsx("flex", "mx-auto")}>
                    <h1
                        className={clsx(
                            "border-r-2",
                            "border-gray-400",
                            "mr-4",
                            "p-2",
                            "pr-4",
                            "text-xl",
                            "font-medium",
                        )}
                    >
                        404
                    </h1>
                    <div className={clsx("text-left", "flex", "items-center")}>
                        <h2 className={clsx("text-xs")}>This page could not be found.</h2>
                    </div>
                </div>
                <NextLink href="/blogs">
                    <Link className={clsx("mt-4")} href="/blogs" icon underline>
                        Go Home
                    </Link>
                </NextLink>
            </div>
        </>
    );
};

export default NotFoundPage;

export const getStaticProps: GetStaticProps<NotFoundPageProps> = async () => {
    try {
        const seoInformation = await getSEOInformation();

        return {
            props: {
                seoInformation,
            },
            revalidate: 3,
        };
    } catch (error) {
        return {
            props: {
                seoInformation: null,
            },
            revalidate: 3,
        };
    }
};
