import React from "react";
import { AppProps } from "next/app";
import { CssBaseline, GeistProvider } from "@geist-ui/react";

import "../styles/global.css";

import "../styles/jsonHighlighter.css";

const App: React.FunctionComponent<AppProps> = ({ Component, pageProps }) => {
    return (
        <GeistProvider>
            <CssBaseline />
            <Component {...pageProps} />
        </GeistProvider>
    );
};

export default App;
