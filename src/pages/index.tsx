import React from "react";
import { Page } from "@geist-ui/react";

const HomePage = () => {
    return (
        <Page size="small">
            <Page.Header>
                <h2>Header</h2>
            </Page.Header>
            <Page.Content>
                <h2>Hello, Everyone.</h2>
                <p>This is a simulated page, you can click anywhere to close it.</p>
            </Page.Content>
            <Page.Footer>
                <h2>Footer</h2>
            </Page.Footer>
        </Page>
    );
};

export default HomePage;
