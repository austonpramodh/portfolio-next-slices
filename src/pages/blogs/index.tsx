import Link from "next/link";
import React from "react";
import { GetStaticProps } from "next";
import clsx from "clsx";
import { Page } from "@geist-ui/react";
import Image from "next/image";

import utilStyles from "../../styles/utils.module.css";
import Date from "../../components/date";
import { getAllPostsWithoutBody, PostWithoutBody } from "../../data/posts";
import { hrefResolver, linkResolver } from "../../utils/linkResolver";
import { getPersonalInfromation, PersonalInformation } from "../../data/personalInformation";
import { getSEOInformation, SEOInformation } from "../../data/seo";
import Footer from "../../components/Footer";
import SEO from "../../components/SEO";
import { imageLoader } from "../../utils/imageLoader";

interface BlogsHomeProps {
    personalInformation: PersonalInformation | null;
    allPostsMeta: PostWithoutBody[];
    seoInformation: SEOInformation | null;
}

const BlogsHome: React.FunctionComponent<BlogsHomeProps> = ({ allPostsMeta, personalInformation, seoInformation }) => {
    if (!personalInformation) return <div>Please setup personal information of Prismic</div>;

    if (!seoInformation) return <div>Please setup seo information of Prismic</div>;

    return (
        <Page render="effect-seo" dotBackdrop size="large">
            <Page.Header className={clsx("pt-4", "md:pt-8", "blog-container", "text-center", "border-b-2")}>
                <SEO
                    siteTitle={seoInformation.data.site_title}
                    pageTitle={"Blog"}
                    keywords={seoInformation.data.meta_keywords.map((k) => k.keyword)}
                />
                {personalInformation.data.profile_picture.url && (
                    <div className={clsx("mx-auto", "mb-4", "rounded")}>
                        <Image
                            loader={imageLoader}
                            className="rounded-full"
                            src={personalInformation.data.profile_picture.url}
                            layout="fixed"
                            height={140}
                            width={140}
                        />
                    </div>
                )}
                <h1 className={clsx("text-3xl", "font-black", "mb-4", "text-4xl", "leading-10")}>
                    {personalInformation.data.name}
                </h1>
                {personalInformation.data.self_introduction && (
                    <p className={clsx("text-base", "font-normal", "text-gray-600", "mb-5")}>
                        {personalInformation.data.self_introduction}
                    </p>
                )}
                <div className={clsx("divide-y")}>
                    <div></div>
                </div>
            </Page.Header>
            <Page.Content className={clsx("pb-20", "md:pb-12")}>
                <section className={clsx(utilStyles.headingMd, utilStyles.padding1px, "blog-container")}>
                    <ul className={clsx("mt-4", "list-none")}>
                        {allPostsMeta.map((p) => {
                            return (
                                <li className={clsx("mb-4", "remove-before")} key={p.id}>
                                    <Link as={linkResolver(p)} href={hrefResolver(p)}>
                                        <a
                                            className={clsx(
                                                "text-2xl",
                                                "font-black",
                                                "leading-9",
                                                "font-lato",
                                                "text-black",
                                            )}
                                        >
                                            {p.data.title}
                                        </a>
                                    </Link>
                                    <br />
                                    <p className={clsx("text-gray-600", "text-sm", "mb-1", "mt-0")}>
                                        <Date dateString={p.last_publication_date} />
                                    </p>
                                    <p className={clsx("text-gray-600", "text-base", "my-1")}>{p.data.introduction}</p>
                                </li>
                            );
                        })}
                    </ul>
                </section>
            </Page.Content>
            <Page.Footer>
                <Footer fullName={personalInformation.data.name} />
            </Page.Footer>
        </Page>
    );
};

export default BlogsHome;

export const getStaticProps: GetStaticProps<BlogsHomeProps> = async () => {
    try {
        const allPostsMeta = await getAllPostsWithoutBody();
        const personalInformation = await getPersonalInfromation();
        const seoInformation = await getSEOInformation();
        return {
            props: {
                allPostsMeta,
                personalInformation,
                seoInformation,
            },
            revalidate: 3,
        };
    } catch (error) {
        //report crashlytics
        return {
            props: {
                allPostsMeta: [],
                revalidate: 60,
                personalInformation: null,
                seoInformation: null,
            },
        };
    }
};
