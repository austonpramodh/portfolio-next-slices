import React from "react";
import { GetStaticPaths, GetStaticProps, InferGetStaticPropsType } from "next";
import { useRouter } from "next/router";
import clsx from "clsx";
import Link from "next/link";
import { Page } from "@geist-ui/react";

import Date from "../../components/date";
import { getPostByUid, getAllPostsWithoutBody, Post } from "../../data/posts";
import SliceZone from "../../slices/SliceZone";
import { getPersonalInfromation, PersonalInformation } from "../../data/personalInformation";
import { getSEOInformation, SEOInformation } from "../../data/seo";
import PageLoading from "../../components/PageLoading";
import Footer from "../../components/Footer";
import SEO from "../../components/SEO";
import JSONDisplay from "../../components/JSONDisplay";

interface PostPageProps {
    post?: Post;
    personalInformation: PersonalInformation | null;
    seoInformation: SEOInformation | null;
}

const PostPage: React.FunctionComponent<InferGetStaticPropsType<typeof getStaticProps>> = ({
    post,
    personalInformation,
    seoInformation,
}) => {
    const router = useRouter();

    if (router.isFallback || !personalInformation || !seoInformation || !post) return <PageLoading />;

    return (
        <>
            <Page>
                <Page.Header className={clsx("pt-8")}>
                    <SEO
                        pageTitle={post.data.short_title}
                        siteTitle={seoInformation.data.site_title}
                        keywords={seoInformation.data.meta_keywords.map((k) => k.keyword)}
                    />
                    <div className={clsx("blog-post-header", "blog-container", "mb-4", "text-gray-500", "text-sm")}>
                        <Link href="/blogs">
                            <a className={clsx("hover:underline", "text-black")}>back to list</a>
                        </Link>
                    </div>
                </Page.Header>
                <Page.Content className={clsx("pt-0", "pb-20", "md:pb-12")}>
                    <article>
                        <section className={clsx("blog-container", "mb-4")}>
                            <h1 className={clsx("text-3xl", "font-extrabold", "tracking-tighter", "my-4")}>
                                {post.data.title}
                            </h1>
                            <div className={clsx("text-gray-500")}>
                                <Date dateString={post.first_publication_date} />
                            </div>
                        </section>
                        <SliceZone slices={post.data.body} />
                    </article>
                </Page.Content>
                <Page.Footer>
                    <Footer fullName={personalInformation.data.name} />
                </Page.Footer>
            </Page>
            <JSONDisplay data={post} />
        </>
    );
};

export default PostPage;

export const getStaticPaths: GetStaticPaths = async () => {
    const allPostsMetaData = await getAllPostsWithoutBody();

    return {
        paths: allPostsMetaData.map((p) => ({ params: { id: p.uid } })),
        fallback: true,
    };
};

export const getStaticProps: GetStaticProps<PostPageProps> = async (context) => {
    const postId = context.params?.id as string | undefined;

    try {
        if (!postId) throw new Error("post id missing - " + postId);

        const post = await getPostByUid(postId);

        const personalInformation = await getPersonalInfromation();

        const seoInformation = await getSEOInformation();

        return {
            props: {
                post: post,
                personalInformation,
                seoInformation,
            },
            revalidate: 3,
            notFound: !post,
        };
    } catch (error) {
        return {
            props: {
                post: null,
                personalInformation: null,
                seoInformation: null,
            },
            revalidate: 3,
            notFound: true,
        };
    }
};
